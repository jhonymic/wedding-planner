package database;

import javax.persistence.*;

@Entity
public class Facilitie {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String description;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;


    public Facilitie() {
    }

    public String getName() {
        return name;
    }

    public Facilitie setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Facilitie setDescription(String description) {
        this.description = description;
        return this;
    }
}
