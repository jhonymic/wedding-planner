package database;


import javax.persistence.*;

@Entity
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int money;

    @OneToOne(mappedBy = "money_id")
    private WeddingPlaner weddingPlaner;

    public Budget() {
    }

    public int getMoney() {
        return money;
    }

    public Budget setMoney(int money) {
        this.money = money;
        return this;
    }
}
