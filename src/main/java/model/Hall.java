package database;

import javax.persistence.*;

@Entity
public class Hall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int numberSeat;
    private int price;

    @OneToOne(mappedBy = "hall_id")
    private WeddingPlaner weddingPlaner;

    public Hall() {
    }

    public int getNumberSeat() {
        return numberSeat;
    }

    public Hall setNumberSeat(int numberSeat) {
        this.numberSeat = numberSeat;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Hall setPrice(int price) {
        this.price = price;
        return this;
    }
}
