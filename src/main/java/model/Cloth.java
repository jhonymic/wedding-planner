package database;

import javax.persistence.*;

@Entity
public class Cloth {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private int price;
    private String size;
    private String brand;

    @OneToOne(mappedBy = "cloth_id")
    private WeddingPlaner weddingPlaner;

    public Cloth() {
    }

    public String getName() {
        return name;
    }

    public Cloth setName(String name) {
        this.name = name;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Cloth setPrice(int price) {
        this.price = price;
        return this;
    }

    public String getSize() {
        return size;
    }

    public Cloth setSize(String size) {
        this.size = size;
        return this;
    }

    public String getBrand() {
        return brand;
    }

    public Cloth setBrand(String brand) {
        this.brand = brand;
        return this;
    }
}
