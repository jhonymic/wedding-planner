package database;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String jazz;
    private String blues;
    private String band;
    private String photoBoth;
    private String mobileBar;
    private String popular;

    @OneToMany(mappedBy = "category")
    List<Facilitie> facilitieList = new ArrayList<>();


    public Category() {
    }

    public String getJazz() {
        return jazz;
    }

    public Category setJazz(String jazz) {
        this.jazz = jazz;
        return this;
    }

    public String getBlues() {
        return blues;
    }

    public Category setBlues(String blues) {
        this.blues = blues;
        return this;
    }

    public String getBand() {
        return band;
    }

    public Category setBand(String band) {
        this.band = band;
        return this;
    }

    public String getPhotoBoth() {
        return photoBoth;
    }

    public Category setPhotoBoth(String photoBoth) {
        this.photoBoth = photoBoth;
        return this;
    }

    public String getMobileBar() {
        return mobileBar;
    }

    public Category setMobileBar(String mobileBar) {
        this.mobileBar = mobileBar;
        return this;
    }

    public String getPopular() {
        return popular;
    }

    public Category setPopular(String popular) {
        this.popular = popular;
        return this;
    }

    public List<Facilitie> getFacilitieList() {
        return facilitieList;
    }

    public Category setFacilitieList(List<Facilitie> facilitieList) {
        this.facilitieList = facilitieList;
        return this;
    }
}
