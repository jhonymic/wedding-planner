package database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class WeddingPlaner {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private Date date;
    private int numberGuest;
    private String location;

    public WeddingPlaner() {
    }

    public Date getDate() {
        return date;
    }

    public WeddingPlaner setDate(Date date) {
        this.date = date;
        return this;
    }

    public int getNumberGuest() {
        return numberGuest;
    }

    public WeddingPlaner setNumberGuest(int numberGuest) {
        this.numberGuest = numberGuest;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public WeddingPlaner setLocation(String location) {
        this.location = location;
        return this;
    }
}
