import dao.*;
import model.*;

public class Main {
    public static void main(String[] args) {

        Budget budget = new Budget(2300);
        BudgetDao budgetDao = new BudgetDaoImpl();
        budgetDao.createBudget(budget);
        Category category = new Category();
        category.setBlues("Blues");
        CategoryDao categoryDao = new CategoryDaoImpl();
        categoryDao.createCategory(category);
        Cloth cloth = new Cloth("Costum", 1500, "m", "Armani");
        ClothDao clothDao = new ClothDaoImpl();
        clothDao.createCloth(cloth);
        Facilitie facilitie = new Facilitie("Bar mobil", "Since 2013", category);
        FacilitieDao facilitieDao = new FacilitieDaoImpl();
        facilitieDao.createFacilitie(facilitie);
        Hall hall = new Hall(800, 2000, "Camarasanu A3");
        HallDao hallDao = new HallDaoImpl();
        hallDao.createHall(hall);
        WeddingPlaner weddingPlaner = new WeddingPlaner("04.07.2020", 300, "Cluj-Napoca");
        weddingPlaner.setBudget(budget);
        weddingPlaner.setCloth(cloth);
        weddingPlaner.setHall(hall);
        WeddingPlanerDao weddingPlanerDao = new WeddingPlanerDaoImpl();
        weddingPlanerDao.createWeddingPlaner(weddingPlaner);

    }
}
